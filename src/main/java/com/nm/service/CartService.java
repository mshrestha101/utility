package com.nm.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.http.MediaType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.nm.exception.UtilityException;
import com.nm.util.ApiProperties;
import com.nm.util.AuthUtil;
import com.nm.util.HttpClientTrustingAllCerts;
import com.nm.util.NMConstants;



public class CartService{

	private static final Logger logger = Logger.getLogger(CartService.class);	

	public String getMiniCart( String requestCookie,  Boolean fromTwoCCCheckout) throws UtilityException{
		HttpResponse response = null;
		BasicCookieStore cookieStore = null;
		String finalResponse = null;
		try {
		  String cookieSession = null;
	    if(requestCookie!=null){
	      cookieSession = requestCookie.replace("\"\"\"\"", "\"\"");
	    }
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpClientTrustingAllCerts object = new HttpClientTrustingAllCerts();
		    httpClient = object.httpClientTrustingAllSSLCerts();
			httpClient.getParams().setParameter("http.protocol.single-cookie-header", true);
			httpClient.getParams().setParameter("http.protocol.cookie-datepatterns", NMConstants.getDegaultDatePatterns());
    		httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY, org.apache.http.client.params.CookiePolicy.BROWSER_COMPATIBILITY);
			System.setProperty("javax.net.ssl.trustStore", NMConstants.JKSPATH);
			System.setProperty("javax.net.ssl.trustStorePassword", "rzipwd");
			System.setProperty("https.protocols", "TLSv1.2");
		
			Gson gson = new Gson();
			if(StringUtils.isNotEmpty(cookieSession)){
				Type listOfCookieObject = new TypeToken<ArrayList<org.apache.http.impl.cookie.BasicClientCookie>>(){}.getType();
			    List<Cookie> cookies2 = gson.fromJson(cookieSession,listOfCookieObject);
			    for(Cookie cookie : cookies2) {
			    	cookieStore = (BasicCookieStore) httpClient.getCookieStore();
			    	cookieStore.addCookie(cookie);
			    }
				httpClient.setCookieStore(cookieStore);
			}
			if(fromTwoCCCheckout != null && fromTwoCCCheckout == true)
			{
				cartload(cookieStore); 
			}
			Date date = new Date();		
			HttpPost getRequest = new HttpPost(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV) + ".MINICART_NEW")+date.getTime());
			getRequest.getParams().setParameter(ClientPNames.COOKIE_POLICY, org.apache.http.client.params.CookiePolicy.BROWSER_COMPATIBILITY);
			getRequest.addHeader(NMConstants.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
			response = httpClient.execute(getRequest);
			cookieStore = (BasicCookieStore) httpClient.getCookieStore();
			String miniCartResponsse =  getResponseData(response,cookieStore);
			JSONObject cartObject = new JSONObject(miniCartResponsse);
			getTotalProductCount(cartObject); 
			finalResponse = cartObject.toString();
		} catch (Exception e) {
			logger.error("error in minicart", e);
		}
		
		if (response == null) {

			throw new UtilityException(500, "Please try again something went wrong!");
		}
		
		return finalResponse;
	}
	
	private void cartload (CookieStore cookieStore){
		
		try{		
		 DefaultHttpClient httpClient = new DefaultHttpClient();
		 httpClient.getParams().setParameter("http.protocol.single-cookie-header", true);
		 httpClient.getParams().setParameter("http.protocol.cookie-datepatterns", NMConstants.getDegaultDatePatterns());
 		 httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY, org.apache.http.client.params.CookiePolicy.BROWSER_COMPATIBILITY);

		 httpClient.setCookieStore(cookieStore);
		 HttpGet getRequest = new HttpGet(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.CHECKOUT));
		 getRequest.getParams().setParameter(ClientPNames.COOKIE_POLICY, org.apache.http.client.params.CookiePolicy.BROWSER_COMPATIBILITY);
		 httpClient.execute(getRequest);

		 }catch (Exception e) {
			 logger.error("Error while calling cart.jsp", e);
		}
	}
	
	private String getResponseData(HttpResponse response,CookieStore cookieStore){
		   String output="";
		   String responseString = "";
		   JSONObject respObj = new JSONObject();
		   BufferedReader br = null;
			try {
			  Gson gson = new Gson();
				String cookieJson = gson.toJson(cookieStore.getCookies());
				logger.info("Add to cart - Response Status code  ----> " + response.getStatusLine().getStatusCode());
				if (response.getStatusLine().getStatusCode() != 200) {
						AuthUtil.sessioninfo(cookieStore);
						JSONArray cookieArray = new JSONArray(cookieJson);
						int cookieArrayLen = cookieArray.length();
						for (int i = 0; i < cookieArrayLen; i++) {
							if (!StringUtils.equalsIgnoreCase(cookieArray.getJSONObject(i).get("name").toString(), "JSESSIONID")) {
							  logger.error("jsessionid: " + cookieArray.getJSONObject(i).toString());
							} else {
								continue;
							}
						}
				    logger.error("Failed : HTTP error code : "
							+ response.getStatusLine().getStatusCode());
				    return respObj.toString();
				}
				br = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "ISO-8859-1"));
		        while ((output = br.readLine()) != null) {
		        	responseString = output;
		        }
		        br.close();
		        GsonBuilder gsonBuilder = new GsonBuilder();
		        gson = gsonBuilder.create();		       
		        String json = gson.toJson(cookieStore.getCookies());
		        respObj = new JSONObject(responseString);
		       
		        String currentCookie;
		        if(json !=null){
		        	currentCookie = json.replace("\"\"\"\"", "\"\"");
					currentCookie = currentCookie.replace("\"\\\"\\\"\"", "\"\"");
		    	}else{
		    		currentCookie = json;
		    	}
		        
		        
		        respObj.put("Cookie",currentCookie);
			} catch (Exception e) {
				logger.error("error in get responsedata", e);
			} 
     return respObj.toString();
	}
	
	private void getTotalProductCount(JSONObject cartObject) {
		int count = 0;
		String countStr="0";
		try {
			if(cartObject.has("MiniCartResp")){
				JSONObject marketingHtml = cartObject.getJSONObject("MiniCartResp");
				String html = marketingHtml.getString("html");
				Document doc = Jsoup.parse(html);
				Elements pElements = doc.select("div.header-drop-down-top > div.float-left > p");
				for (Element parEle :pElements) {
					Elements spanElements= parEle.getElementsByTag("span");
					for(Element spanElement : spanElements){
						String spanText=spanElement.text();
						 if(spanText.contains("items in your bag")||spanText.contains("item in your bag")){
							 countStr= spanText.replace("items in your bag","").replace("item in your bag","").trim();
							 count = Integer.parseInt(countStr);
							 break;
						 }
							 
					}
					
				}
				int bagCount = 0;
		        Elements tableElements = doc.select("table");
		        Elements tableRowElements = tableElements.select("tr");
		        for (int i = 1; i < tableRowElements.size(); i++) {
	                Element row = tableRowElements.get(i);
	                Elements rowItems = row.select("tr");
	                for (Element rowItem : rowItems) {
	                    String qty = Jsoup.parse( rowItem.select( "p:contains(Qty:)").hasText()?rowItem.select( "p:contains(Qty:)").get(0).toString():"").text().replace("Qty:", "").trim();
	                    if(StringUtils.isNotEmpty(qty)){
	                    	bagCount += Integer.parseInt(qty);
	                    }
	                 }
		        }
				cartObject.put("currentBagCount",bagCount);
				cartObject.put("totalProductCount",count);
			}			
		} catch (Exception e) {
			logger.error("error in getting total product count", e);
		}
		
	}
}