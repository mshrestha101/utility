package com.nm.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.nm.configuration.CacheData;
import com.nm.dao.impl.FeedBackDaoImpl;
import com.nm.dao.impl.IdfaDaoImpl;
import com.nm.model.CircleInfo;
import com.nm.model.FeedBack;
import com.nm.model.FeedBackSaveData;
import com.nm.model.GiftFinder;
import com.nm.model.State;
import com.nm.model.URLs;
import com.nm.model.ValidateTimestamp;
import com.nm.util.ApiProperties;
import com.nm.util.NMConstants;
import com.sun.jersey.api.client.ClientResponse.Status;

@Component
@Path("/")
public class UtilityContoller {
	@Autowired
	private CacheData  cacheResponse;
	
	@Autowired
	private FeedBackDaoImpl feedBackDaoImpl;
	
	@Autowired
	IdfaDaoImpl idfaDaoImpl;
	

	
	private String cacheKey = "StoreKeys";
	
	private static final Logger log = Logger.getLogger(UtilityContoller.class);
	@GET
	@Path("/getUrls")
	@Produces(MediaType.APPLICATION_JSON)
	public String getUrls(@Context UriInfo info, @QueryParam("appVersion") String appVersion){
		String url = info.getRequestUri().toString();
		String cacheData = cacheResponse.cacheData(url,null,ApiProperties.getUrlTTL());
		if(cacheData !=null){
			return cacheData;
		}
		URLs urls = new URLs();
		urls.setAppID(ApiProperties.getMsg(NMConstants.APPID));
		String response=null;
		try{
			// business logic to get the InCircle content pages
			urls.setShop("https://www.neimanmarcus.com?type=CFA");
			CircleInfo circleInfo = new CircleInfo();
			circleInfo.setCircleOne("https://s3.amazonaws.com/nmcfa/incirclehtmls/circleOne.html");
			circleInfo.setCircleTwo("https://s3.amazonaws.com/nmcfa/incirclehtmls/circleTwo.html");
			circleInfo.setCircleThree("https://s3.amazonaws.com/nmcfa/incirclehtmls/circleThree.html");
			circleInfo.setCircleFour("https://s3.amazonaws.com/nmcfa/incirclehtmls/circleFour.html");
			circleInfo.setCircleFive("https://s3.amazonaws.com/nmcfa/incirclehtmls/circleFive.html");
			circleInfo.setCircleSix("https://s3.amazonaws.com/nmcfa/incirclehtmls/circleSix.html");
			circleInfo.setPresidentCircle("https://s3.amazonaws.com/nmcfa/incirclehtmls/presidentsCircle.html");
			circleInfo.setChairmanCircle("https://s3.amazonaws.com/nmcfa/incirclehtmls/chairmanCircle.html");
			urls.setCircleContent(circleInfo);
			urls.setAutoSuggest(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.AUTO_SUGGEST));
			urls.setMiniCart(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.MINICART));
			urls.setMinCartCurrency(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.MINICART_CURRENCY));
			urls.setCheckOut(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.CHECKOUT));
			urls.setCheckOutLogin(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.CHECKOUT_LOGIN));
			urls.setWishListShare(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.WISHLIST_SOCIAL));
			urls.setCheckoutCss(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.CHECKOUTCSS));
			urls.setSlyceUrl(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.SLYCEURL));
			urls.setSurveyDay(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.SURVEYDAY));
			urls.setExpressCheckoutText(ApiProperties.getMsg(NMConstants.EXPRESS_CHECKOUT_TEXT));
			urls.setEnableExpressCheckout(ApiProperties.getMsg(NMConstants.ENABLE_EXPRESS_CHECKOUT));
			String webViewUrl = ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.WEBVIEWURLS);
			String[] webViewUrls = webViewUrl.split(",");
			List<String> webViewList = new ArrayList<>();
				for(String wurl: webViewUrls){
					webViewList.add(wurl);
				}
			urls.setWebViewUrls(webViewList);
			String indicatorUrl = ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.INDICATORURLS);
			String[] indicatorUrls = indicatorUrl.split(",");
			List<String> indicator = new ArrayList<>();
				for(String indUrl: indicatorUrls){
					indicator.add(indUrl);
				}
			urls.setIndicatorUrls(indicator);
			urls.setSlyceKey(ApiProperties.getMsg(NMConstants.SLYCEKEY)); 
			urls.setDisplayHolidayHours(ApiProperties.getMsg(NMConstants.DISPLAY_HOLIDAY_HOURS));
			urls.setNetworkProfile(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.networkProfile));
			urls.setSsid(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.ssid));
			urls.setMyNM(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.MYNM));
			urls.setXleftMaxCount(5);
			urls.setBackgroundRadius(Double.toString(ApiProperties.getUserInstoreAvailabilityRadius()));
			urls.setGeoFenceRefreshTime("1800");
			urls.setStoreDetailsRefreshTime("86400");
			urls.setFavoriteItem(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.favoriteItem));
			urls.setPayMyBill(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.payMyBill));
			urls.setIncircleNeedHelp(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.incircleNeedHelp));
			urls.setIncircleApplyNow(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.incircleApplyNow));
			urls.setOrderHistory(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.orderHistory));
			urls.setAddressBook(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.addressBook));
			urls.setPaymentInfo(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.paymentInfo));
			urls.setTwoCCOrderReviewUrl(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.twoCCOrderReviewUrl));
			urls.setExpressCheckoutUrl(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.expressCheckoutURL));
			urls.setEditAccountURL(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.editAccountURL));
			urls.setWebViewEnviromentURL(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.webViewEnviromentURL));
			urls.setIncallNumber(ApiProperties.getMsg(NMConstants.INCALL_NUMBER));
			String twoCCFlowURL = ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.TWOCCFLOWURLS);
			String[] twoCCFlowURLs = twoCCFlowURL.split(",");
			List<String> twoCCFlowList = new ArrayList<>();
					for(String ccUrl: twoCCFlowURLs){
						twoCCFlowList.add(ccUrl);
					}
			urls.setTwoCCFlowURLs(twoCCFlowList);
			List<String> checkoutWebviewList = new ArrayList<>();
			String checkoutWebviewURL = ApiProperties.getMsg(NMConstants.WEBCHECKOUTSTYLES);
					if(checkoutWebviewURL != null){
						String[] checkoutWebviewURLs = checkoutWebviewURL.split("/");
						if(checkoutWebviewURLs != null){
							for(String webStyle: checkoutWebviewURLs){
								checkoutWebviewList.add(webStyle);
							}
							urls.setCheckOutWebviewStyles(checkoutWebviewList);
						}
					}
			urls.setAssistanceUrl(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.ASSISTANCE_URL));
			String puchoutUrls = ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV)+NMConstants.PUNCHOUT_WEBVIEW_URLS);
	        	if(puchoutUrls != null && !puchoutUrls.isEmpty()){
  					String[] punchOutWebViewUrls = puchoutUrls.split(",");
  	        List<String> punchOutWebViewList = new ArrayList<>();
  	        		for(String powurl: punchOutWebViewUrls){
  	        				punchOutWebViewList.add(powurl);
  	        		}
  			urls.setPunchOutWebViewUrls(punchOutWebViewList);
	        }
	        	/** properties for FlipToFind*/
	        	String flipToFind = ApiProperties.getMsg(NMConstants.FLIP_TO_FIND);
				String[] split = StringUtils.split(flipToFind,"~");
				GiftFinder gift = new GiftFinder();
				gift.setCount(split[1]);
				gift.setCategoryId(split[2]);	
				gift.setTitle(split[3]);
				urls.setGiftFinder(gift);
	        	
		response = new Gson().toJson(urls);
		}catch (Exception e) {
			response = "Error";
			log.error("Error",e);			
		}
		cacheResponse.cacheData(url,response,ApiProperties.getUrlTTL());
		return response;		
	}
	
	@GET
	@Path("/getProfanityWords")
	@Produces(MediaType.APPLICATION_JSON)
	public String getProfanityWords(@Context UriInfo info){
		String url = info.getRequestUri().toString();
		String cacheData = cacheResponse.cacheData(url,null,ApiProperties.getUrlTTL());
		if(cacheData !=null){
			return cacheData;
		}
		String profanityWordsStr =ApiProperties.getMsg("PROFANITY_WORDS");
		String profanityWordsTimeStamp =ApiProperties.getMsg("PROFANITY_WORDS_TIMESTAMP");
		JSONObject response = new JSONObject();

		try{
			String[] profanity = profanityWordsStr.split(",");
			response.put("profanityWordsStr", profanity);
			response.put("timestamp", profanityWordsTimeStamp);
			}catch (Exception e) {
				log.error("Error",e);			
			}
			cacheResponse.cacheData(url,response.toString(),ApiProperties.getUrlTTL());
			return response.toString();
		}
	@GET
	@Path("/getStateCodes")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStateCodes(@Context UriInfo info) {
		String url = info.getRequestUri().toString();
		String cacheData = cacheResponse.cacheData(url,null,ApiProperties.getStoreTTL());
		if(cacheData !=null){
			return Response.status(Status.OK)
					.entity(cacheData).type(MediaType.TEXT_PLAIN).build();
		}
		List<State> statesCodesList= null;
		JSONArray stateCodeArray = new JSONArray();
		JSONObject storeObj = new JSONObject();	
		try {
			statesCodesList = idfaDaoImpl.getStateCodesList();
		
		for(State stateCode : statesCodesList ){
			
			JSONObject stateCodes = new JSONObject();	
			stateCodes.put("stateName",stateCode.getStateName());
			stateCodes.put("stateCode",stateCode.getStateCode());
			stateCodeArray.put(stateCodes);
			
		}
			storeObj.put("stateCodes", stateCodeArray);
		} catch (JSONException e) {
		  log.error("json exception in getStateCodes", e);
		}catch (Exception e) {
		  log.error("exception in getStateCodes", e);
			return Response.status(NMConstants.DB_ERROR_CODE)
					.entity("Problem In Fetching the data from DataBase ...").type(MediaType.TEXT_PLAIN).build();
		}
		cacheResponse.cacheData(url,storeObj.toString(),ApiProperties.getStoreTTL());
		cacheResponse.storeCacheKeys(cacheKey,url, ApiProperties.getStoreTTL());
		return Response.status(Status.OK)
				.entity(storeObj.toString()).type(MediaType.TEXT_PLAIN).build();
	}

	@GET
	@Path("/getFeedBack")
	@Produces("application/json")
	public String getFeedBackQuestions(@Context UriInfo info){
		String url = info.getRequestUri().toString();
		String cacheData = cacheResponse.cacheData(url,null,ApiProperties.getFeedBackTTL());
		if(cacheData !=null){
			return cacheData;
		}
		List<FeedBack> feedBackQuestions = new ArrayList<>();
		JSONObject response = new JSONObject();
		net.minidev.json.JSONArray array = new net.minidev.json.JSONArray();
		try{
			feedBackQuestions = feedBackDaoImpl.getFeedBackQuestions();
			for(FeedBack feeedBack : feedBackQuestions){
				JSONObject object = new JSONObject();
				net.minidev.json.JSONArray arrayAnswers = new net.minidev.json.JSONArray();
				if((feeedBack.getAnswers()==null || "null".equalsIgnoreCase(feeedBack.getAnswers()))|| feeedBack.getAnswers().isEmpty()){
					object.put("Question",feeedBack.getQuestion());
					object.put("Answers",arrayAnswers);
				}else{
				String[] answers = feeedBack.getAnswers().split(",");
				for(int i=0;i<answers.length;i++){
					arrayAnswers.add(answers[i]);}
				object.put("Question",feeedBack.getQuestion());
				object.put("Answers",arrayAnswers);
				}
				array.add(object);
			}
			response.put("FeedBackQuestions", array);
		}catch(Exception e ){
			log.info(NMConstants.EXCEPTION+e);
		}
		cacheResponse.cacheData(url,response.toString(),ApiProperties.getFeedBackTTL());
		return response.toString();
	}
	
	@POST
	@Path("/saveFeedBack")
	public String saveFeedBackDetails(FeedBackSaveData feedBackData){
		String status = "";
		if(feedBackData.getDeviceId().trim().length()==0){
			return "Invlaid Device Id";
		}
		try{
			status = feedBackDaoImpl.feedBackDataSave(feedBackData);
		}catch(Exception e ){
			log.info(NMConstants.EXCEPTION +e);
		}
		return status;
	}
	
	@GET
	@Path("/saveIdfaId")
	public String saveIdfaId(@QueryParam("id") String id) 
	{
		String result=null;
		try
		{
			log.info("IDFA Service");
			log.info("IDFA ID"+id);
			result = idfaDaoImpl.idfaInsert(id);
		}
		catch(Exception e)
		{
			log.error(e);
		}
		
		return result;
	}
	
	@GET
	@Path("/getNetworkTypes")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNetworkTypes() throws JSONException {
		
		JSONArray networkTypeArray = new JSONArray();
		JSONObject networkTypeObj = new JSONObject();	
		
		JSONObject networkType = new JSONObject();
		networkType.put("cardName","Neiman Marcus");
		networkType.put("cardCode","Neiman Marcus");
		networkTypeArray.put(networkType);
		
		networkType = new JSONObject();	
		networkType.put("cardName","Bergdorf Goodman");
		networkType.put("cardCode","Bergdorf Goodman");
		networkTypeArray.put(networkType);

		networkType = new JSONObject();	
		networkType.put("cardName","American Express");
		networkType.put("cardCode","American Express");
		networkTypeArray.put(networkType);

		
		networkType = new JSONObject();	
		networkType.put("cardName","Visa");
		networkType.put("cardCode","Visa");
		networkTypeArray.put(networkType);
			
		networkType = new JSONObject();	
		networkType.put("cardName","MasterCard");
		networkType.put("cardCode","MasterCard");
		networkTypeArray.put(networkType);

		networkType = new JSONObject();	
		networkType.put("cardName","Discover");
		networkType.put("cardCode","Discover");
		networkTypeArray.put(networkType);
		
		networkTypeObj.put("cardCodes", networkTypeArray);
			
		return Response.ok().entity(networkTypeObj.toString()).type(MediaType.TEXT_PLAIN).build();
			
	}
		

	@GET
	@Path("/getTimeStamps")
	@Produces(MediaType.APPLICATION_JSON)
	public String getArticlesFeed(@HeaderParam("X-Forwarded-For") String xForwarder) throws JSONException {
		String[] ipArray = xForwarder.split(",");
		ValidateTimestamp validateTimestamp = feedBackDaoImpl.validateTimestampApi(ipArray[0]);
		if(validateTimestamp.getIpAddress()==null){
			feedBackDaoImpl.insertTimestampAPi(ipArray[0]);
		}else{
			feedBackDaoImpl.updateTimestampApi(ipArray[0]);
		}
		JSONObject timeStamps = new JSONObject();
		try{
		String cacheData = cacheResponse.cacheData(NMConstants.TIMESTAMP, null, ApiProperties.getTimeStamps());
		if (cacheData != null) {
			if(cacheData.contains("homeBanner")){
				return cacheData;
			}
		}
		timeStamps.put("clp", "");
		timeStamps.put("blog", "");
		timeStamps.put("store", "");
		timeStamps.put("homeBanner", "");
		cacheResponse.cacheData(NMConstants.TIMESTAMP, timeStamps.toString(), ApiProperties.getTimeStamps());
		}catch (Exception e) {
			log.error("Error in Reload Service", e);
		}
		return timeStamps.toString();
	}
	
	@GET
	@Path("/getData")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWebViewData(@Context UriInfo info) throws JSONException {
		  String url = info.getRequestUri().toString();
		    String cacheData = cacheResponse.cacheData(url, null, ApiProperties.getPLPTTL());
		    if (cacheData != null)
		      return cacheData;
		JSONObject resp = new JSONObject();
		JSONArray executionArray = new JSONArray();
		JSONArray skipArray = new JSONArray();
		try{
			executionArray.put("document.getElementsByTagName('header')[0].style.display = 'none';");
			executionArray.put("document.getElementsByClassName('mobile-header')[0].style.display = 'none';");
			executionArray.put("document.getElementsByClassName('nm-text-banner')[0].style.display = 'none';");
			executionArray.put("document.getElementsByClassName('nm-text-banner')[1].style.display = 'none';");
			executionArray.put("document.getElementsByClassName('nm-text-banner')[2].style.display = 'none';");
			executionArray.put("document.getElementsByClassName('copyright')[0].style.display = 'none';");
			executionArray.put("document.getElementsByTagName('footer')[0].style.display = 'none';");
			executionArray.put("document.getElementById('brandheader').style.display = 'none';");
			executionArray.put("document.getElementsByClassName('trans-silo')[0].style.display = 'none';");
			executionArray.put("document.getElementsByClassName('footer')[0].style.display = 'none';");
			executionArray.put("document.getElementById('footerSilo').style.display = 'none';");
			executionArray.put("document.getElementById('br-container').style.display = 'none';");
			executionArray.put("document.getElementsByClassName('slick-slide')[0].style.display = 'none';");
			executionArray.put("document.getElementsByClassName('mobile-promo-header hide-on-desktop hide-on-tablet slick-initialized slick-slider')[0].style.display = 'none';");
			executionArray.put("document.getElementsByTagName('brandheader')[0].style.display = 'none';");
			executionArray.put("document.getElementById('cartPromo2desktop').style.display = 'none';");
			executionArray.put("document.getElementById('myfavorites').style.display = 'none';");
			executionArray.put("document.getElementById('mobile-search-form').style.display = 'none';");

			
			resp.put("executionArray", executionArray);
			 
			 skipArray.put("fls.doubleclick.net");
			 skipArray.put("content.shoprunner.com");
			 skipArray.put("about:blank");
			 skipArray.put("api.bam-x.com");
			 skipArray.put("widgets.stores.neimanmarcus.com");
			 skipArray.put("vop.sundaysky.com");
			 skipArray.put("loadus.exelator.com");
			 skipArray.put("login.dotomi.com");
			 skipArray.put("rs.gwallet.com");
			 skipArray.put("platform.twitter.com");
			 skipArray.put("staticxx.facebook.com");
			 skipArray.put("https://n-cdn.areyouahuman.com");
			 resp.put("skipArray", skipArray);
			 
			 //ipadWebview properties
			 JSONObject ipadWebView = new JSONObject();
			 ipadWebView.put("cartCountTag", "document.getElementsByClassName(\"mobile-icon-bag\")[0].innerHTML");
			 ipadWebView.put("cartResponseTag", "MiniCartResp");
			 resp.put("ipadWebView", ipadWebView);
			 cacheResponse.cacheData(url, resp.toString(), ApiProperties.getPushDetailsTTL());
		}catch (Exception e) {
			log.error("error in WebView Info",e);
			resp.put("errorMessage", "Error in getting Data");
		}
		 return resp.toString();
	  }
	
	 @GET
	  @Path("/getHideProperties")
	 @Produces(MediaType.APPLICATION_JSON)
	  public String getHideProperties(@Context UriInfo info, @HeaderParam("cookieSession") String cookieSession) throws JSONException{
		JSONObject resp = new JSONObject();
		JSONArray executionArray = new JSONArray();
		JSONArray cartHideProperties = new JSONArray();
		JSONArray skipArray = new JSONArray();
		JSONArray skipDestinationInfo = new JSONArray();
		try{		 
			executionArray.put("document.getElementsByTagName('header')[0].style.display = 'none';");
			executionArray.put("document.getElementsByClassName('mobile-header')[0].style.display = 'none';");
			executionArray.put("document.getElementsByClassName('nm-text-banner')[0].style.display = 'none';");
			executionArray.put("document.getElementsByClassName('nm-text-banner')[1].style.display = 'none';");
			executionArray.put("document.getElementsByClassName('nm-text-banner')[2].style.display = 'none';");
			executionArray.put("document.getElementsByClassName('copyright')[0].style.display = 'none';");
			executionArray.put("document.getElementsByTagName('footer')[0].style.display = 'none';");
			executionArray.put("document.getElementById('brandheader').style.display = 'none';");
			executionArray.put("document.getElementsByClassName('trans-silo')[0].style.display = 'none';");
			executionArray.put("document.getElementsByClassName('footer')[0].style.display = 'none';");
			executionArray.put("document.getElementById('footerSilo').style.display = 'none';");
			executionArray.put("document.getElementById('br-container').style.display = 'none';");
			executionArray.put("document.getElementsByClassName('slick-slide')[0].style.display = 'none';");
			executionArray.put("document.getElementsByClassName('mobile-promo-header hide-on-desktop hide-on-tablet slick-initialized slick-slider')[0].style.display = 'none';");
			executionArray.put("document.getElementsByTagName('brandheader')[0].style.display = 'none';");
			executionArray.put("document.getElementById('cartPromo2desktop').style.display = 'none';");
			executionArray.put("document.getElementById('myfavorites').style.display = 'none';");
			executionArray.put("document.getElementById('mobile-search-form').style.display = 'none';");
			 executionArray.put("document.getElementById('promotop')[0].className = 'hide'");
			 executionArray.put("document.getElementById('myfavorites')[0].className = 'hide'");		 
			 executionArray.put("document.getElementsByClassName('mobile-promo-header hide-on-desktop hide-on-tablet slick-initialized slick-slider')[0].className = 'hide'");
			 executionArray.put("document.getElementsByClassName('mobile-header')[0].className = 'hide'");
			 executionArray.put("document.getElementsByTagName('header')[0].className = 'hide'");
			 executionArray.put("document.getElementsByTagName('footer')[0].className = 'hide'");
			 executionArray.put("document.getElementsByClassName('silo-nav')[0].className = 'hide'");
			 executionArray.put("var child1 = document.getElementById('myfavorites-hdr');  child1.className = 'hide';");
			 executionArray.put("var child1 = document.getElementById('myfavorites-pg'); var child2 = child1.getElementsByClassName('emailMyFavList hide-on-mobile')[0]; child2.className = 'hide';");
			 executionArray.put("var child1 = document.getElementById('myNMContainer'); var child2 = child1.getElementsByClassName('promo-top')[0]; child2.className = 'hide';");
			 executionArray.put("var child1 = document.getElementsByClassName('myNMFavNav grid-15 tablet-grid-20 hide-on-mobile')[0];  var child2 = child1.getElementsByClassName('nav')[0]; var child3 = child2.getElementsByTagName('li')[0]; child3.className = 'hide'");
			 executionArray.put("var child1 = document.getElementsByClassName('myNMFavNav grid-15 tablet-grid-20 hide-on-mobile')[0];  var child2 = child1.getElementsByClassName('nav')[0]; var child3 = child2.getElementsByTagName('li')[1]; child3.className = 'hide'");
			 executionArray.put("var child1 = document.getElementsByClassName('myNMFavNav grid-15 tablet-grid-20 hide-on-mobile')[0];  var child2 = child1.getElementsByClassName('nav')[0]; var child3 = child2.getElementsByTagName('li')[3]; child3.className = 'hide'");
			 executionArray.put("var child1 = document.getElementsByClassName('myNMFavNav grid-15 tablet-grid-20 hide-on-mobile')[0];  var child2 = child1.getElementsByClassName('nav')[0]; var child3 = child2.getElementsByTagName('li')[4]; child3.className = 'hide'");
			 executionArray.put("var child1 = document.getElementsByClassName('myNMFavNav grid-15 tablet-grid-20 hide-on-mobile')[0];  var child2 = child1.getElementsByClassName('nav')[0]; var child3 = child2.getElementsByTagName('li')[5]; child3.className = 'hide'");
			 executionArray.put("var child1 = document.getElementsByClassName('myNMFavNav grid-15 tablet-grid-20 hide-on-mobile')[0];  var child2 = child1.getElementsByClassName('nav')[0]; var child3 = child2.getElementsByTagName('li')[6]; child3.className = 'hide'");
			 executionArray.put("var child1 = document.getElementsByClassName('myNMFavNav grid-15 tablet-grid-20 hide-on-mobile')[0];  var child2 = child1.getElementsByClassName('nav')[0]; var child3 = child2.getElementsByTagName('li')[7]; child3.className = 'hide'");
			 executionArray.put("var child1 = document.getElementsByClassName('myNMFavNav grid-15 tablet-grid-20 hide-on-mobile')[0];  var child2 = child1.getElementsByClassName('nav')[0]; var child3 = child2.getElementsByTagName('li')[8]; child3.className = 'hide'");
			 executionArray.put("var child1 = document.getElementsByClassName('myNMFavNav grid-15 tablet-grid-20 hide-on-mobile')[0];  var child2 = child1.getElementsByClassName('nav')[0]; var child3 = child2.getElementsByTagName('li')[9]; child3.className = 'hide'");
			 executionArray.put("var child1 = document.getElementsByClassName('myNMFavNav grid-15 tablet-grid-20 hide-on-mobile')[0];  var child2 = child1.getElementsByClassName('nav')[0]; var child3 = child2.getElementsByTagName('li')[10]; child3.className = 'hide'");
			 executionArray.put("var child1 = document.getElementsByClassName('grid-container entire-site-container make-relative')[0];  var child2 =child1.getElementsByTagName('main')[0]; var child3 = child2.getElementsByClassName('mobile-grid-100 tablet-grid-80 grid-85')[0]; var child4 = child3.getElementsByClassName('hide-on-desktop hide-on-tablet ')[0]; child4.className = 'hide'");
			 executionArray.put("var a = document.getElementsByClassName('figure-hotspot')[1];a.href=''");
			 executionArray.put("document.getElementById('myfavorites').style.display = 'none';");
			 executionArray.put("document.getElementById('mobile-search-form').style.display = 'none';");
			 executionArray.put("document.getElementsByTagName('header')[0].style.display = 'none';");
				executionArray.put("document.getElementsByClassName('nm-text-banner')[0].style.display = 'none';");
				executionArray.put("document.getElementsByClassName('nm-text-banner')[1].style.display = 'none';");
				executionArray.put("document.getElementsByClassName('nm-text-banner')[2].style.display = 'none';");
				executionArray.put("document.getElementsByClassName('copyright')[0].style.display = 'none';");
				executionArray.put("document.getElementById('brandheader').style.display = 'none';");
				executionArray.put("document.getElementsByClassName('trans-silo')[0].style.display = 'none';");
				executionArray.put("document.getElementsByClassName('footer')[0].style.display = 'none';");
				executionArray.put("document.getElementById('footerSilo').style.display = 'none';");
				executionArray.put("document.getElementById('br-container').style.display = 'none';");
				executionArray.put("document.getElementsByClassName('slick-slide')[0].style.display = 'none';");
				executionArray.put("document.getElementsByClassName('mobile-promo-header hide-on-desktop hide-on-tablet slick-initialized slick-slider')[0].style.display = 'none';");
				executionArray.put("document.getElementsByTagName('brandheader')[0].style.display = 'none';");
				executionArray.put("document.getElementById('cartPromo2desktop').style.display = 'none';");
				executionArray.put("document.getElementById('myfavorites').style.display = 'none';");
				executionArray.put("document.getElementById('mobile-search-form').style.display = 'none';");
				executionArray.put("document.getElementById('fantasy-gifts-single-social-icons').style.display = 'none';");
				
				
			 resp.put("executionArray", executionArray);
			 resp.put("webviewHide", ApiProperties.getMsg(NMConstants.WEBVIEWHIDE));
			 skipArray.put("fls.doubleclick.net");
			 skipArray.put("content.shoprunner.com");
			 skipArray.put("about:blank");
			 skipArray.put("api.bam-x.com");
			 skipArray.put("widgets.stores.neimanmarcus.com");
			 skipArray.put("vop.sundaysky.com");
			 skipArray.put("loadus.exelator.com");
			 skipArray.put("login.dotomi.com");
			 skipArray.put("rs.gwallet.com");
			 skipArray.put("platform.twitter.com");
			 skipArray.put("staticxx.facebook.com");
			 skipArray.put("bid.g.doubleclick.net");
			 skipArray.put("dis.us.criteo.com");
			 skipArray.put("n-cdn.areyouahuman.com");
			 resp.put("skipArray", skipArray);
			 
			 skipDestinationInfo.put("cat48140738_cat000672_cat000000/c.cat");
			 skipDestinationInfo.put("cat56140878/c.cat");
			 skipDestinationInfo.put("cat56140879/c.cat");
			 skipDestinationInfo.put("cat56140880/c.cat");
			 skipDestinationInfo.put("cat56140881/c.cat");
			 skipDestinationInfo.put("cat56140882/c.cat");
			 skipDestinationInfo.put("cat56140883/c.cat");
			 skipDestinationInfo.put("cat56140884/c.cat");
			 skipDestinationInfo.put("cat56140885/c.cat");
			 skipDestinationInfo.put("cat56140886/c.cat");
			 skipDestinationInfo.put("cat56140887/c.cat");
			 resp.put("skipDestinationInfo", skipDestinationInfo);
			 
			 
			 cartHideProperties.put("document.getElementsByTagName('header')[0].style.display = 'none';");
			 cartHideProperties.put("document.getElementsByClassName('mobile-header')[0].style.display = 'none';");
			 cartHideProperties.put("document.getElementsByClassName('nm-text-banner')[0].style.display = 'none';");
			 cartHideProperties.put("document.getElementsByClassName('nm-text-banner')[1].style.display = 'none';");
			 cartHideProperties.put("document.getElementsByClassName('nm-text-banner')[2].style.display = 'none';");
			 cartHideProperties.put("document.getElementsByClassName('ymal')[0].style.display = 'none';");
			 cartHideProperties.put("document.getElementsByClassName('copyright')[0].style.display = 'none';");
			 cartHideProperties.put("document.getElementsByTagName('footer')[0].style.display = 'none';");
			 cartHideProperties.put("document.getElementById('ymalContainer').style.display = 'none';");
			 cartHideProperties.put("document.getElementById('brandheader').style.display = 'none';");
			 cartHideProperties.put("document.getElementsByClassName('trans-silo')[0].style.display = 'none';");
			 cartHideProperties.put("document.getElementsByClassName('footer')[0].style.display = 'none';");
			 cartHideProperties.put("document.getElementById('footerSilo').style.display = 'none';");
			 cartHideProperties.put("document.getElementById('br-container').style.display = 'none';");
			 cartHideProperties.put("document.getElementsByClassName('slick-slide')[0].style.display = 'none';");
			 cartHideProperties.put("document.getElementsByClassName('mobile-promo-header hide-on-desktop hide-on-tablet slick-initialized slick-slider')[0].style.display = 'none';");
			 cartHideProperties.put("document.getElementById('elem-silo').style.display = 'none';");
			 cartHideProperties.put("document.getElementsByTagName('brandheader')[0].style.display = 'none';");
			 cartHideProperties.put("document.getElementById('cartPromo2desktop').style.display = 'none’;");
			 cartHideProperties.put("document.getElementsByClassName('nm-checkout_flow1_banner')[0].style.display = 'none';");
			 resp.put("cartHideProperties", cartHideProperties);
			 JSONObject ipadWebView = new JSONObject();
			 ipadWebView.put("cartCountTag", "document.getElementsByClassName(\"mobile-icon-bag\")[0].innerHTML");
			 ipadWebView.put("cartResponseTag", "MiniCartResp");
			 resp.put("ipadWebView", ipadWebView);
			 CartService cartService = new CartService();
			 String cartResponse = cartService.getMiniCart(cookieSession,false);
			 JSONObject cartJson = new JSONObject(cartResponse);
			 if(cartJson.has("Cookie")){
				 resp.put("Cookie", cartJson.get("Cookie"));
			 }
		}catch (Exception e) {
			log.error("Error in MyNM Service", e);
			resp.put("errorMessage", "Error in getting Data");
		}
		 return resp.toString();
	  }
	 
	 @GET
	  @Path("/logJailbrokenDevice")
	 @Produces(MediaType.APPLICATION_JSON)
	  public Response getHideProperties(@QueryParam("deviceId") String deviceId){
		log.error("JailBroken Device ID: "+deviceId);
		JSONObject response = new JSONObject();
		try{
			response.put("msg", "success");
			return Response.status(Response.Status.OK).entity(response.toString()).build();	
		}catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(response.toString()).build();
		}
		
	 }
	 
}