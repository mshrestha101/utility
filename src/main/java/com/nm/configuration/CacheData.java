package com.nm.configuration;

import org.springframework.stereotype.Component;

@Component
public interface CacheData {

	 String cacheData(String url, String cacheValue, int time);
	 void storeCacheKeys(String cacheKey, String url, int time);
}
