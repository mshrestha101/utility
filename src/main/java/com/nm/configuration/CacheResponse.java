package com.nm.configuration;
/* 
 * File Name : CacheResponse.java 
 * Purpose   : CacheResponse 
 * Author    : PhotonInfotech
 * Date      : 19-December-2016 
 */

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nm.util.ApiProperties;

@Component
public class CacheResponse implements CacheData{

	private static Logger log = Logger.getLogger(CacheResponse.class);
	
	@Autowired
	TokenCache memcacheImpl;
	
	private String getKey(String url) {
		String cacheKey =null;
		try{
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(url.getBytes("iso-8859-1"), 0, url.length());
			cacheKey = new BigInteger(1,md.digest()).toString(16);
		}catch(Exception e){
			log.error("exception while digesting key", e);
		}
		 return cacheKey;
	}
	
	@Override
	public String cacheData(String url, String cacheValue, int time){
		String cacheKey;
		String result = null;
		cacheKey = getKey(url);
		if(cacheValue != null){
			if("true".equalsIgnoreCase(ApiProperties.getMsg("isCacheForStore")))
			{
				try{
					memcacheImpl.set(cacheKey, cacheValue, time);
				}catch(Exception e){
					log.error("Cache Server Connection failure to set data into cache...", e);
				}
			}
		}else{			
			try{
				if("true".equalsIgnoreCase(ApiProperties.getMsg("isCacheForStore")) && memcacheImpl.isAuthenticKey(cacheKey)){
							result = memcacheImpl.get(cacheKey).toString();
				}
				}catch(Exception e){
					log.error("Cache Server Connection failure to get data from cache...", e);
					result = null;
					return result;
				}
		}
		return result;
	}

	@Override
	public void storeCacheKeys(String cacheKey, String url,int time) {
		ArrayList<String> listOfKeys = new ArrayList<>();
		String urlKey = getKey(url);
		if(  memcacheImpl.isAuthenticKey(cacheKey)){
			try{
				listOfKeys = (ArrayList<String>) memcacheImpl.get(cacheKey);
				listOfKeys.add(urlKey);
				memcacheImpl.setStoreKeys(cacheKey, listOfKeys, time);
			}catch(Exception e){
				log.error("Cache Server Connection failure to set data into cache...", e);
			}
		}else{
			listOfKeys.add(urlKey);
			memcacheImpl.setStoreKeys(cacheKey, listOfKeys, time);
		}
	}
}
