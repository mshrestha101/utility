package com.nm.configuration;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public interface TokenCache {

	/**
     * to authenticate the given key in cache
     * 
     * @param key
     * @return
     * @throws Exception
     * @throws IOException
     */
    boolean isAuthenticKey(String key);

    /**
     * @param key
     * @return
     * @throws Exception
     * @throws IOException
     */
    Object get(String key);

        
    /**
     * @param rziAuthObject
     * @param cacheKey
     * @return
     * @throws Exception
     * @throws IOException
     */
    void set(String cacheKey, String jsonString, int time);
    void setStoreKeys(String cacheKey, List<String> storeKeys, int time);

    boolean delete(String key);
}
