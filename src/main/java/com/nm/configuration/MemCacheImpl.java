package com.nm.configuration;
/* 
 * File Name : MemCacheImpl.java 
 * Purpose   : MemCacheImpl 
 * Author    : PhotonInfotech
 * Date      : 19-December-2016 
 */

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.List;

import net.spy.memcached.MemcachedClient;

import org.springframework.stereotype.Component;

@Component
public class MemCacheImpl implements TokenCache{

	private MemcachedClient client = null;
	
	public MemCacheImpl() throws IOException {
	  if(System.getProperty("cfa_cache_enabled") != null && "true".equalsIgnoreCase(System.getProperty("cfa_cache_enabled"))){
   	 this.client = new MemcachedClient(new InetSocketAddress(System.getProperty("memcacheURL"),
               Integer.parseInt("11211")));
	  }
   }
	
	
    public TokenCache getInstance() {
        return this;
    }

    /*
     * checking for valid Key in cache
     */
    @Override
    public boolean isAuthenticKey(String key) {

    	if(this.client != null){
	        Object rziUserAuthKey = this.client.get(key);
	        if (rziUserAuthKey != null) {
	            return true;
	        }
	        return false;
    	}else{
    		return false;
    	}

    }

    /*
     * returns object value of the cache key
     */
    @Override
    public Object get(String key) {
    	if(this.client != null){
    		return this.client.get(key);
    	}else{
    		return null;
    	}
    }

    /*
     * store the object value and returns the key
     */
    @Override
    public void set(String cacheKey, String jsonString, int time) {
    	if(this.client != null){
    		this.client.set(cacheKey, time, jsonString);
    	}
    }
    
    @Override
    public void setStoreKeys(String cacheKey, List<String> storeKeys, int time) {
    	if(this.client != null){
    		this.client.set(cacheKey, time, storeKeys);
    	}
    }

    /* Delete the key from the cache */
    @Override
    public boolean delete(String key) {
    	if(this.client != null){
    		this.client.delete(key);
    		return true;
    	}else{
    		return false;
    	}
    }

}
