package com.nm.exception;

public class UtilityException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int status;
	private String message;

	public UtilityException(int status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return "UtilityException [status=" + status + ", message=" + message + "]";
	}
	
	

}
