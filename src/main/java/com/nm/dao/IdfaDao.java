package com.nm.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.nm.model.State;

@Component
public interface IdfaDao {
  public String idfaInsert(String id);
  public List<State> getStateCodesList();
}
