package com.nm.dao;

import java.util.List;

import com.nm.model.FeedBack;
import com.nm.model.FeedBackSaveData;
import com.nm.model.ValidateTimestamp;

public interface FeedBackDao {
	
	 List<FeedBack> getFeedBackQuestions();
	 String feedBackDataSave(FeedBackSaveData feedBackData);
	 String insertTimestampAPi(String ipAddress);
	 ValidateTimestamp validateTimestampApi(String ipAddress);
	 String updateTimestampApi(String ipAddress);
}
