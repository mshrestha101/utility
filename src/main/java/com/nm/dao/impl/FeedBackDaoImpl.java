package com.nm.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

import com.nm.dao.FeedBackDao;
import com.nm.model.FeedBack;
import com.nm.model.FeedBackSaveData;
import com.nm.model.ValidateTimestamp;
import com.nm.model.mapper.FeedBackMapper;
import com.nm.model.mapper.TimestampMapper;
import com.nm.util.CategoryUtil;
import com.nm.util.NMConstants;

public class FeedBackDaoImpl implements FeedBackDao {

	private static final Logger log = Logger.getLogger(FeedBackDaoImpl.class);
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplateObject = new JdbcTemplate(dataSource);
	}

	@Override
	public List<FeedBack> getFeedBackQuestions() {
		List<FeedBack> feedBack = new ArrayList<>();
		try {
			String query = "SELECT * FROM NM_FEEDBACK_QUESTIONS";
			feedBack = jdbcTemplateObject.query(query, new FeedBackMapper());
		} catch (Exception exe) {
			log.info(NMConstants.EXCEPTION + exe);
			log.fatal("Error", exe);
		}
		return feedBack;
	}

	@Override
	public String feedBackDataSave(FeedBackSaveData feedBackData) {
		List<FeedBack> feedBackList = feedBackData.getFeedBack();
		String status = "";
		CategoryUtil util = new CategoryUtil();
		try {
			for (FeedBack feedBack : feedBackList) {
				String sql = "INSERT INTO NM_FEEDBACK_DETAILS VALUES('" + feedBackData.getDeviceId() + "','"
						+ feedBack.getQuestion() + "','" + feedBack.getAnswers() + "','" + util.getCSTTime() + "')";
				int result = jdbcTemplateObject.update(sql);

				if (result == 1) {
					status = "success";
				} else {
					status = "Failed to store Data...";
				}
			}
		} catch (Exception exe) {
			log.info(NMConstants.EXCEPTION + exe);
			log.fatal("Error", exe);
		}
		return status;
	}

	@Override
	public String insertTimestampAPi(String ipAddress) {
		String status = "N";
		try{
		String sql = "INSERT INTO NM_CFAValidation (IPAddress,timestampApi) values('"+ipAddress+"','Y')";
		int result = jdbcTemplateObject.update(sql);

		if (result == 1) {
			status = "Y";
		} else {
			status = "N";
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	@Override
	public ValidateTimestamp validateTimestampApi(String ipAddress) {
		ValidateTimestamp validateTimestamp = new ValidateTimestamp();
		List<ValidateTimestamp> object = new ArrayList<>();
		try {
			String query = "select * from NM_CFAValidation where IPAddress='"+ipAddress+"';";
			object = jdbcTemplateObject.query(query, new TimestampMapper());
			for(ValidateTimestamp timestamp : object){
				validateTimestamp.setIpAddress(timestamp.getIpAddress());
				validateTimestamp.setTimeStamp(timestamp.getTimeStamp());
			}
			
		} catch (Exception exe) {
			exe.printStackTrace();
			log.info(NMConstants.EXCEPTION + exe);
			log.fatal("Error", exe);
		}
		return validateTimestamp;
	}

	@Override
	public String updateTimestampApi(String ipAddress) {
		String status = "N";
		try{
		String sql = "update NM_CFAValidation set timestampApi='Y' where IPAddress='"+ipAddress+"';";
		int result = jdbcTemplateObject.update(sql);

		if (result == 1) {
			status = "Y";
		} else {
			status = "N";
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

}
