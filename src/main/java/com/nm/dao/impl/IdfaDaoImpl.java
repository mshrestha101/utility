package com.nm.dao.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.nm.dao.IdfaDao;
import com.nm.model.State;
import com.nm.model.mapper.StateCodeMapper;
import com.nm.util.NMConstants;

@Repository
public class IdfaDaoImpl implements IdfaDao{

  private static final Logger log = Logger.getLogger(IdfaDaoImpl.class);


  private JdbcTemplate jdbcTemplateObject;

  public void setDataSource(DataSource dataSource) {
    this.jdbcTemplateObject = new JdbcTemplate(dataSource);
  }
  @Override
  public String idfaInsert(String id) {
    try {
      log.info("IDFA Id " + id);
      if(StringUtils.isNotEmpty(id)){
        String sql = "SELECT count(*) FROM NM_IDFA WHERE idfa_id = ?";
        int count = jdbcTemplateObject.queryForObject(sql, new Object[] { id }, Integer.class);
        log.info("IDFA Exist Count " + count);
        if (count < 1) {
          
          Date d = new Date(System.currentTimeMillis());
          DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
          df.setTimeZone(TimeZone.getTimeZone("CST"));
          String serverTime = df.format(d);
          
          String query="INSERT INTO NM_IDFA VALUES('"+id+"','"+serverTime+"', '0')";
          log.info("IDFA Query " + query);
          jdbcTemplateObject.update(query);  
        }
      }else{
        log.info("IDFA Id is empty") ;
      }
      
    }
    catch (Exception e) {
      log.error("Error in inserting IDFA", e);
    }
    return "SUCCESS";
  }
  @Override
	public List<State> getStateCodesList(){
		log.info("StroeDaoImpl ------- getStateCodesList() method starts");
		List<State> stateCodesList = new ArrayList<>();
		try{
			String query = NMConstants.SS_GET_STATE_CODES;
			stateCodesList = jdbcTemplateObject.query(query, new StateCodeMapper());
		}catch (DataAccessException e) {
			log.fatal(NMConstants.SQL_EXCEPTION, e);
		}
		log.info("StroeDaoImpl ------- getStateCodesList() method ends");
		return stateCodesList;
	}
  
}
