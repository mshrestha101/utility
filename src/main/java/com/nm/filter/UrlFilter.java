package com.nm.filter;

/* 
 * File Name : UrlFilter.java 
 * Purpose   : UrlFilter 
 * Author    : PhotonInfotech
 * Date      : 19-December-2016 
 */
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;

public class UrlFilter implements Filter {

	private static Logger log = Logger.getLogger(UrlFilter.class);
	@SuppressWarnings("unused")
	private FilterConfig filterConfig = null;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	@SuppressWarnings("unused")
	private static class ResettableStreamHttpServletRequest extends HttpServletRequestWrapper {

		private byte[] rawData;
		private HttpServletRequest request;
		private ResettableServletInputStream servletStream;

		public ResettableStreamHttpServletRequest(HttpServletRequest request) {
			super(request);
			this.request = request;
			this.servletStream = new ResettableServletInputStream();
		}

		public void resetInputStream() {
			servletStream.stream = new ByteArrayInputStream(rawData);
		}

		@Override
		public ServletInputStream getInputStream() throws IOException {
			if (rawData == null) {
				rawData = IOUtils.toByteArray(this.request.getReader());
				servletStream.stream = new ByteArrayInputStream(rawData);
			}
			return servletStream;
		}

		@Override
		public BufferedReader getReader() throws IOException {
			if (rawData == null) {
				rawData = IOUtils.toByteArray(this.request.getReader());
				servletStream.stream = new ByteArrayInputStream(rawData);
			}
			return new BufferedReader(new InputStreamReader(servletStream));
		}

		private class ResettableServletInputStream extends ServletInputStream {

			private InputStream stream;

			@Override
			public int read() throws IOException {
				return stream.read();
			}
		}
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
			throws java.io.IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		if (StringUtils.isEmpty(request.getHeader("accept"))) {
			response.setStatus(406);
			String errorResponse = getErrorResponse("406", "Not Acceptable", "406 Not Acceptable");
			response.getWriter().write(errorResponse);
			return;
		}

		try {
			chain.doFilter(request, response);
		} catch (Exception e) {
			log.error("Exception in filter : "+e);
			String errorResponse = getErrorResponse("500", "Please try again something went wrong!",
					"500 Internal Server Error");
			response.getWriter().write(errorResponse);
			return;
		}
	}

	private String getErrorResponse(String code, String message, String status) {
		JSONObject errorObject = new JSONObject();
		try {
			JSONObject object = new JSONObject();
			object.put("code", code);
			object.put("message", message);
			object.put("status", status);
			errorObject.put("error", object);
		} catch (Exception e2) {
			log.error("Exception in filter", e2);
		}
		return StringEscapeUtils.escapeHtml(errorObject.toString()).replaceAll("&quot;", "\"");
	}

	@Override
	public void destroy() {

	}

}