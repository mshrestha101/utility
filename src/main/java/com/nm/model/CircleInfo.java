package com.nm.model;

public class CircleInfo {
	public String circleOne;
	public String circleTwo;
	public String circleThree;
	public String circleFour;
	public String circleFive;
	public String circleSix;
	public String presidentCircle;
	public String chairmanCircle;
	public String getCircleOne() {
		return circleOne;
	}
	public void setCircleOne(String circleOne) {
		this.circleOne = circleOne;
	}
	public String getCircleTwo() {
		return circleTwo;
	}
	public void setCircleTwo(String circleTwo) {
		this.circleTwo = circleTwo;
	}
	public String getCircleThree() {
		return circleThree;
	}
	public void setCircleThree(String circleThree) {
		this.circleThree = circleThree;
	}
	public String getCircleFour() {
		return circleFour;
	}
	public void setCircleFour(String circleFour) {
		this.circleFour = circleFour;
	}
	public String getCircleFive() {
		return circleFive;
	}
	public void setCircleFive(String circleFive) {
		this.circleFive = circleFive;
	}
	public String getCircleSix() {
		return circleSix;
	}
	public void setCircleSix(String circleSix) {
		this.circleSix = circleSix;
	}
	public String getPresidentCircle() {
		return presidentCircle;
	}
	public void setPresidentCircle(String presidentCircle) {
		this.presidentCircle = presidentCircle;
	}
	public String getChairmanCircle() {
		return chairmanCircle;
	}
	public void setChairmanCircle(String chairmanCircle) {
		this.chairmanCircle = chairmanCircle;
	}

}
