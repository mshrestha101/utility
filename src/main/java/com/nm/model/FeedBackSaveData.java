package com.nm.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FeedBackSaveData {

	private String deviceId;
	
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public List<FeedBack> getFeedBack() {
		return feedBack;
	}
	public void setFeedBack(List<FeedBack> feedBack) {
		this.feedBack = feedBack;
	}
	private List<FeedBack> feedBack;
}
