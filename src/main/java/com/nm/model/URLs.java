package com.nm.model;

import java.util.List;

public class URLs {

	private String autoSuggest;
	private String miniCart;
	private String minCartCurrency;
	private String checkOut;
	private String checkOutLogin;
	private String wishListShare;
	private String checkoutCss;
	private String surveyDay;
	private List<String> webViewUrls;
	private String slyceKey;
	private String slyceUrl;
	private String displayHolidayHours;
	private String networkProfile;
	private String ssid;
	private String myNM;
	private int xleftMaxCount;
	private String backgroundRadius;
	private String geoFenceRefreshTime;
	private String storeDetailsRefreshTime;
	private String favoriteItem;
	private String payMyBill;
	private String incircleNeedHelp;
	private String incircleApplyNow;
	private String orderHistory;
	private String addressBook;
	private List<String> indicatorUrls;
	private String expressCheckoutText;
	private String enableExpressCheckout;
	private String twoCCOrderReviewUrl;
	private String expressCheckoutUrl;
	private String editAccountURL;
	private String webViewEnviromentURL;
	private List<String> twoCCFlowURLs;
	private List<String> checkOutWebviewStyles;
	private String assistanceUrl;
	private List<String> punchOutWebViewUrls;
	private CircleInfo circleContent;
	private String appID;
	private String incallNumber;
	private String paymentInfo;
	private String shop;
    private GiftFinder giftFinder;
	
	public List<String> getCheckOutWebviewStyles() {
		return checkOutWebviewStyles;
	}
	public void setCheckOutWebviewStyles(List<String> checkOutWebviewStyles) {
		this.checkOutWebviewStyles = checkOutWebviewStyles;
	}
	public String getSurveyDay() {
		return surveyDay;
	}
	public void setSurveyDay(String surveyDay) {
		this.surveyDay = surveyDay;
	}
	public String getAutoSuggest() {
		return autoSuggest;
	}
	public void setAutoSuggest(String autoSuggest) {
		this.autoSuggest = autoSuggest;
	}
	public String getMiniCart() {
		return miniCart;
	}
	public void setMiniCart(String miniCart) {
		this.miniCart = miniCart;
	}
	public String getMinCartCurrency() {
		return minCartCurrency;
	}
	public void setMinCartCurrency(String minCartCurrency) {
		this.minCartCurrency = minCartCurrency;
	}
	public String getCheckOut() {
		return checkOut;
	}
	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}
	public String getCheckOutLogin() {
		return checkOutLogin;
	}
	public void setCheckOutLogin(String checkOutLogin) {
		this.checkOutLogin = checkOutLogin;
	}
	public String getWishListShare() {
		return wishListShare;
	}
	public void setWishListShare(String wishListShare) {
		this.wishListShare = wishListShare;
	}
	public String getCheckoutCss() {
		return checkoutCss;
	}
	public void setCheckoutCss(String checkoutCss) {
		this.checkoutCss = checkoutCss;
	}
	public List<String> getWebViewUrls() {
		return webViewUrls;
	}
	public void setWebViewUrls(List<String> webViewUrls) {
		this.webViewUrls = webViewUrls;
	}
	public String getSlyceKey() {
		return slyceKey;
	}
	public void setSlyceKey(String slyceKey) {
		this.slyceKey = slyceKey;
	}
	public String getSlyceUrl() {
		return slyceUrl;
	}
	public void setSlyceUrl(String slyceUrl) {
		this.slyceUrl = slyceUrl;
	}
	public String getDisplayHolidayHours() {
		return displayHolidayHours;
	}
	public void setDisplayHolidayHours(String displayHolidayHours) {
		this.displayHolidayHours = displayHolidayHours;
	}
	public String getNetworkProfile() {
		return networkProfile;
	}
	public void setNetworkProfile(String networkProfile) {
		this.networkProfile = networkProfile;
	}
	public String getSsid() {
		return ssid;
	}
	public void setSsid(String ssid) {
		this.ssid = ssid;
	}
	public String getMyNM() {
		return myNM;
	}
	public void setMyNM(String myNM) {
		this.myNM = myNM;
	}
	public int getXleftMaxCount() {
		return xleftMaxCount;
	}
	public void setXleftMaxCount(int xleftMaxCount) {
		this.xleftMaxCount = xleftMaxCount;
	}
	public String getBackgroundRadius() {
		return backgroundRadius;
	}
	public void setBackgroundRadius(String backgroundRadius) {
		this.backgroundRadius = backgroundRadius;
	}
	public String getGeoFenceRefreshTime() {
		return geoFenceRefreshTime;
	}
	public void setGeoFenceRefreshTime(String geoFenceRefreshTime) {
		this.geoFenceRefreshTime = geoFenceRefreshTime;
	}
	public String getStoreDetailsRefreshTime() {
		return storeDetailsRefreshTime;
	}
	public void setStoreDetailsRefreshTime(String storeDetailsRefreshTime) {
		this.storeDetailsRefreshTime = storeDetailsRefreshTime;
	}
	public String getFavoriteItem() {
		return favoriteItem;
	}
	public void setFavoriteItem(String favoriteItem) {
		this.favoriteItem = favoriteItem;
	}
	public String getPayMyBill() {
		return payMyBill;
	}
	public void setPayMyBill(String payMyBill) {
		this.payMyBill = payMyBill;
	}
	public String getIncircleNeedHelp() {
		return incircleNeedHelp;
	}
	public void setIncircleNeedHelp(String incircleNeedHelp) {
		this.incircleNeedHelp = incircleNeedHelp;
	}
	public String getIncircleApplyNow() {
		return incircleApplyNow;
	}
	public void setIncircleApplyNow(String incircleApplyNow) {
		this.incircleApplyNow = incircleApplyNow;
	}
	public String getOrderHistory() {
		return orderHistory;
	}
	public void setOrderHistory(String orderHistory) {
		this.orderHistory = orderHistory;
	}
	public String getAddressBook() {
		return addressBook;
	}
	public void setAddressBook(String addressBook) {
		this.addressBook = addressBook;
	}
	public String getPaymentInfo() {
		return paymentInfo;
	}
	public void setPaymentInfo(String paymentInfo) {
		this.paymentInfo = paymentInfo;
	}
		public List<String> getIndicatorUrls() {
		return indicatorUrls;
	}
	public void setIndicatorUrls(List<String> indicatorUrls) {
		this.indicatorUrls = indicatorUrls;
	}

	public String getExpressCheckoutText() {
		return expressCheckoutText;
	}
	public void setExpressCheckoutText(String expressCheckoutText) {
		this.expressCheckoutText = expressCheckoutText;
	}
	public String getEnableExpressCheckout() {
		return enableExpressCheckout;
	}
	public void setEnableExpressCheckout(String enableExpressCheckout) {
		this.enableExpressCheckout = enableExpressCheckout;
	}
	
	public String getTwoCCOrderReviewUrl() {
		return twoCCOrderReviewUrl;
	}
	public void setTwoCCOrderReviewUrl(String twoCCOrderReviewUrl) {
		this.twoCCOrderReviewUrl = twoCCOrderReviewUrl;
	}
	public String getExpressCheckoutUrl() {
		return expressCheckoutUrl;
	}
	public void setExpressCheckoutUrl(String expressCheckoutUrl) {
		this.expressCheckoutUrl = expressCheckoutUrl;
	}
	public String getEditAccountURL() {
		return editAccountURL;
	}
	public void setEditAccountURL(String editAccountURL) {
		this.editAccountURL = editAccountURL;
	}
	public String getWebViewEnviromentURL() {
		return webViewEnviromentURL;
	}
	public void setWebViewEnviromentURL(String webViewEnviromentURL) {
		this.webViewEnviromentURL = webViewEnviromentURL;
	}
	public List<String> getTwoCCFlowURLs() {
		return twoCCFlowURLs;
	}
	public void setTwoCCFlowURLs(List<String> twoCCFlowURLs) {
		this.twoCCFlowURLs = twoCCFlowURLs;
	}
  public String getAssistanceUrl() {
    return assistanceUrl;
  }
  public void setAssistanceUrl(String assistanceUrl) {
    this.assistanceUrl = assistanceUrl;
  }
  public List<String> getPunchOutWebViewUrls() {
    return punchOutWebViewUrls;
  }
  public void setPunchOutWebViewUrls(List<String> punchOutWebViewUrls) {
    this.punchOutWebViewUrls = punchOutWebViewUrls;
  }

public CircleInfo getCircleContent() {
	return circleContent;
}

public void setCircleContent(CircleInfo circleContent) {
	this.circleContent = circleContent;
}

public String getAppID() {
	return appID;
}

public void setAppID(String appID) {
	this.appID = appID;
}

public String getIncallNumber() {
	return incallNumber;
}

public void setIncallNumber(String incallNumber) {
	this.incallNumber = incallNumber;
}
public String getShop() {
	return shop;
}
public void setShop(String shop) {
	this.shop = shop;
}
public GiftFinder getGiftFinder() {
	return giftFinder;
}
public void setGiftFinder(GiftFinder giftFinder) {
	this.giftFinder = giftFinder;
}
}