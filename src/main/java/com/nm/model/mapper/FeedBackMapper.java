package com.nm.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.nm.model.FeedBack;

public class FeedBackMapper implements RowMapper<FeedBack> {

	@Override
	public FeedBack mapRow(ResultSet results, int rowNum) throws SQLException {
		FeedBack feedBack = new FeedBack();
		feedBack.setQuestion(results.getString("QUESTION"));
		feedBack.setAnswers(results.getString("ANSWER"));
		return feedBack;
	}

}
