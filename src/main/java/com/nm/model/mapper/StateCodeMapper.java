package com.nm.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.nm.model.State;

public class StateCodeMapper implements RowMapper<State> {

	@Override
	public State mapRow(ResultSet results, int rowNum) throws SQLException {
		
		State stateInfo = new State();
		stateInfo.setStateName(results.getString("STATE_NAME"));
		stateInfo.setStateCode(results.getString("STATE_CODE"));
		return stateInfo;
	}
}
