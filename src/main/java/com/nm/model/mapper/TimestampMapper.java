package com.nm.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.nm.model.ValidateTimestamp;

public class TimestampMapper implements RowMapper<ValidateTimestamp> {

	@Override
	public ValidateTimestamp mapRow(ResultSet results, int rowNum) throws SQLException {
		ValidateTimestamp validateTimestampObject = new ValidateTimestamp();
		validateTimestampObject.setIpAddress(results.getString("IPAddress"));
		validateTimestampObject.setTimeStamp(results.getString("timestampApi"));
		return validateTimestampObject;
	}

}
