package com.nm.util;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.nm.exception.UtilityException;

@Component
@Provider
public class UtilityExceptionMapper implements javax.ws.rs.ext.ExceptionMapper<UtilityException>{

	private static Logger log = Logger.getLogger(UtilityExceptionMapper.class);
	@Override
	public Response toResponse(UtilityException exception) {
		log.error("Exception occurred while processing  : "+exception);
		JSONObject errorObject = new JSONObject();
		JSONObject object = new JSONObject();
		if (exception.getStatus() == 500 ) {
		try {
			object.put("code", exception.getStatus());
			object.put("message",exception.getMessage() );
			errorObject.put("error", object);
		} catch (JSONException e) {
			log.error("Exception in UtilityExceptionMapper :",e);
		}
		}
		return Response.status(Response.Status.OK).entity(errorObject.toString()).type(MediaType.TEXT_PLAIN).build();
	}

}
