package com.nm.util;

import java.util.Arrays;
import java.util.List;

import org.apache.http.impl.cookie.DateUtils;

public class NMConstants {  

  /**
   * Cart Service Constants
   */
  
  public static final String APPID="cfaApiKey";
  public static final String ACCEPT = "accept";
  public static final String EXCEPTION= "exception";
  public static final String CFA_ENV="cfa_env";
  public static final String  AUTO_SUGGEST= ".AUTO_SUGGEST";
  public static final String MINICART= ".MINICART";
  public static final String MINICART_CURRENCY = ".MINICART_CURRENCY";
  public static final String CHECKOUT_LOGIN = ".CHECKOUT_LOGIN";
  public static final String CHECKOUT = ".CHECKOUT";
  public static final String WISHLIST_SOCIAL =".WISHLIST_SOCIAL";
  public static final String CHECKOUTCSS =".CHECKOUT_CSS_NEW";
  public static final String SLYCEURL= ".SLYCEURL";
  public static final String ABETESTING_SHOP_URL = ".ABETesting_SHOP_URL";
  public static final String SURVEYDAY = ".SURVEYDAY";
  public static final String WEBVIEWURLS = ".WEBVIEWURLS";
  public static final String EXPRESS_CHECKOUT_TEXT="EXPRESS_CHECKOUT_TEXT";
  public static final String ENABLE_EXPRESS_CHECKOUT="ENABLE_EXPRESS_CHECKOUT";
  public static final String INDICATORURLS = ".INDICATORURLS";
  public static final String SLYCEKEY= "SLYCEKEY";
  public static final String DISPLAY_HOLIDAY_HOURS = "DISPLAY_HOLIDAY_HOURS";
  public static final String networkProfile = ".NetworkProfile";
  public static final String ssid = ".SSID";
  public static final String MYNM=".MYNM";
  public static final String favoriteItem = ".favoriteItem";
  public static final String payMyBill = ".payMyBill";
  public static final String incircleNeedHelp = ".incircleNeedHelp"; 
  public static final String incircleApplyNow = ".incircleApplyNow";
  public static final String orderHistory = ".orderHistory"; 
  public static final String addressBook = ".addressBook"; 
  public static final String paymentInfo = ".paymentInfo";
  public static final String twoCCOrderReviewUrl = ".twoCCOrderReviewUrl";
  public static final String expressCheckoutURL = ".expressCheckoutURL";
  public static final String editAccountURL = ".editAccountURL";
  public static final String webViewEnviromentURL = ".webViewEnviromentURL";
  public static final String  WEBVIEWHIDE = "WEBVIEW_HIDE_STR";
  public static final String TWOCCFLOWURLS = ".twoCCFlowURLs";
  public static final String WEBCHECKOUTSTYLES = "WEBCHECKOUTSTYLES";
  public static final String ASSISTANCE_URL = ".ASSISTANCEURL";
  public static final String PUNCHOUT_WEBVIEW_URLS = ".PUNCHOUTWEBVIEWURLS";
  public static final String TIMESTAMP = "TimeStamps";
  public static final int DB_ERROR_CODE = 503;
  public static final String SQL_EXCEPTION= "sql exception";
  public static final String SS_GET_STATE_CODES     = "SELECT * FROM NM_STATE_CODES ORDER BY STATE_CODE ASC";
  public static final String UTF = "UTF-8";
  public static final String INCALL_NUMBER = "incallNumber";
  public static final String UTF8 = "UTF-8";
  public static final String CBPUNCHOUTIND = "cbPunchoutIndicator";
  public static final String JKSPATH = "/opt/keystore/afa/afa.jks";
  public static final String  FLIP_TO_FIND = "FLIP_TO_FIND";

  public static final List<String> getDegaultDatePatterns(){
	    return DEFAULT_DATE_PATTERNS;
	    
  }
  protected static final List<String> DEFAULT_DATE_PATTERNS = Arrays.asList(
          DateUtils.PATTERN_RFC1123,
          DateUtils.PATTERN_RFC1036,
          DateUtils.PATTERN_ASCTIME,
          "EEE, dd-MMM-yyyy HH:mm:ss z",
          "EEE, dd-MMM-yyyy HH-mm-ss z",
          "EEE, dd MMM yy HH:mm:ss z",
          "EEE dd-MMM-yyyy HH:mm:ss z",
          "EEE dd MMM yyyy HH:mm:ss z",
          "EEE dd-MMM-yyyy HH-mm-ss z",
          "EEE dd-MMM-yy HH:mm:ss z",
          "EEE dd MMM yy HH:mm:ss z",
          "EEE,dd-MMM-yy HH:mm:ss z",
          "EEE,dd-MMM-yyyy HH:mm:ss z",
          "EEE, dd-MM-yyyy HH:mm:ss z",
          "EEE, dd-MMM-yyyy HH:mm:ss X"
  );
  private NMConstants(){
    
  }
}
