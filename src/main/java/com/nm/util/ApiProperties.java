package com.nm.util;

import java.util.Properties;

public class ApiProperties {

	private static Properties properties;

    private ApiProperties() {
    }

    public static Properties setInstance(Properties propertiesArgs) {
        properties = propertiesArgs;
        return properties;
    }

    /**
     * This method will return value of specified property key
     * 
     * @param key
     * @return
     */
    public static String getMsg(String key) {
        return properties.getProperty(key);
    }
    public static int getPLPTTL(){
    	return Integer.parseInt(properties.getProperty("PLP_TTL"));
    }
    public static int getStoreTTL(){
    	return Integer.parseInt(properties.getProperty("STORE_TTL"));
    }
    public static int getFeedBackTTL(){
    	return Integer.parseInt(properties.getProperty("FEEDBACK_TTL"));
    }
    public static int getUrlTTL(){
    	return Integer.parseInt(properties.getProperty("URL_TTL"));
    }
    public static int getTimeStamps(){
    	return Integer.parseInt(properties.getProperty("TIME_STAMP"));
    }    
    // push message additional details service TTL
    public static int getPushDetailsTTL(){
    	return Integer.parseInt(properties.getProperty("PUSH_DETAILS_TTL"));
    }
    //user in store
    public static double getUserInstoreAvailabilityRadius(){
    	return Double.parseDouble(properties.getProperty("USERINSTORERADIUS"));
    }    
}
