package com.nm.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

public class AuthUtil {

	/**
	 * @param args
	 */
	private static Logger log = Logger.getLogger(AuthUtil.class);
	 public static String sessioninfo (CookieStore cookieStore){
		    
		    try{    
		     DefaultHttpClient httpClient = new DefaultHttpClient();
		     httpClient.getParams().setParameter("http.protocol.single-cookie-header", true);
		     httpClient.getParams().setParameter("http.protocol.cookie-datepatterns", NMConstants.DEFAULT_DATE_PATTERNS);
		     httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY, org.apache.http.client.params.CookiePolicy.BROWSER_COMPATIBILITY);
		     httpClient.setCookieStore(cookieStore);
		     HttpGet getRequest = new HttpGet(ApiProperties.getMsg(System.getProperty(NMConstants.CFA_ENV) + ".NMO_URL")+"/sessioninfo.jsp");
		     HttpResponse response = httpClient.execute(getRequest);
		       BufferedReader br =  new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		     StringBuilder tempResponse = new StringBuilder();
		     String line;
		     while ((line = br.readLine()) != null)
		     {
		        tempResponse.append(line);
		        tempResponse.append('\r');
		     }
		       log.error("SESSIONINFO : " + tempResponse.toString());     
		       br.close();
		       return tempResponse.toString();
		     }catch (Exception e) {
		       log.error("Error while getting sessioninfo", e);
		    }
		    return "session info not available";
		  }
	 
	 private AuthUtil(){
		 
	 }
}

