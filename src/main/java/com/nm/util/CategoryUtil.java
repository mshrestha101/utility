package com.nm.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class CategoryUtil {
	
	public String getCSTTime() {
    Calendar currentdate = Calendar.getInstance();
    DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    TimeZone obj = TimeZone.getTimeZone("America/Chicago");
    formatter.setTimeZone(obj);
    String cstTime = formatter.format(currentdate.getTime());
    // we have to return time in MYSQL db format 
    return cstTime.replace('/', '-');
  }

}
