package com.nm.util;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
@Component
@Provider
public class UnsupportedMediaTypeMapper  implements ExceptionMapper<WebApplicationException> {
	/**
	 * 
	 */
	private static Logger log = Logger.getLogger(UnsupportedMediaTypeMapper.class);

	@Override
    public Response toResponse(WebApplicationException e) {
		
		String errorResponse=null;
		log.error("---------------------------------------Not Supported Exception ------------------------ " + e.getClass().getName());
		log.error("Exception in UnsupportedMediaTypeMapper : "+e);
		if (e.getResponse().getStatus() == 415 ) {
			errorResponse = getErrorResponse("406", "Not Acceptable", "406 Not Acceptable");
			return Response.status(406).entity(errorResponse).build();
		}
		errorResponse = getErrorResponse("500", "Please try again something went wrong!", "500 Internal Server Error");
		return Response.status(500).entity(errorResponse).build();
    }
	
	private String getErrorResponse(String code, String message, String status) {
		JSONObject errorObject = new JSONObject();
		try {
			JSONObject object = new JSONObject();
			object.put("code", code);
			object.put("message", message);
			object.put("status", status);
			errorObject.put("error", object);
		} catch (Exception e2) {
			log.error("Exception in filter", e2);
		}
		return StringEscapeUtils.escapeHtml(errorObject.toString()).replaceAll("&quot;", "\"");
	}
}
