package com.nm.cfa.controller;

import static org.junit.Assert.assertEquals;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.http.impl.client.BasicCookieStore;
import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.nm.configuration.CacheData;
import com.nm.configuration.TokenCache;
import com.nm.exception.UtilityException;
import com.nm.model.FeedBack;
import com.nm.model.FeedBackSaveData;
import com.nm.model.State;
import com.nm.model.URLs;
import com.nm.service.CartService;
import com.nm.service.UtilityContoller;
import com.nm.util.AuthUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:junit-cfa-servlet.xml" })
public class TestFeedBackService extends TestCase{
	private static Logger log = Logger.getLogger(TestFeedBackService.class);
	@Autowired
	UtilityContoller service;
	
	@Autowired
	TokenCache memcacheImpl;
	
	@Autowired
	CartService cartService;
	
	UriInfo uriInfo;
	
	CacheData cacheData;
	@Before
	public void setUp(){
		uriInfo = EasyMock.createMock(UriInfo.class);
		cacheData = EasyMock.createMock(CacheData.class);
	}
	@Test
	public void testGetFeedBackData() throws NoSuchAlgorithmException, UnsupportedEncodingException, URISyntaxException{
		EasyMock.expect(uriInfo.getRequestUri()).andReturn(new java.net.URI("https://localhost:8080/feedback/getFeedBack"));
		EasyMock.replay(uriInfo);
		EasyMock.expect(cacheData.cacheData("https://localhost:8080/feedback/getFeedBack", null, 21600)).andReturn(null);
		EasyMock.replay(cacheData);
		String result = service.getFeedBackQuestions(uriInfo);
		String url = "https://localhost:8080/feedback/getFeedBack";
		String cacheKey = null;
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(url.getBytes("iso-8859-1"), 0, url.length());
		cacheKey = new BigInteger(1,md.digest()).toString(16);
		memcacheImpl.delete(cacheKey);
		assertEquals(true, result.contains("FeedBackQuestions"));
	}
	@Test
	public void testSaveFeedBackData() {
		FeedBackSaveData feedBackData = new FeedBackSaveData();
		FeedBack feedBack = new FeedBack();
		feedBack.setAnswers("Example Answer-1");
		feedBack.setQuestion("Example Question-1");
		List<FeedBack> feedBackList = new ArrayList<FeedBack>();
		feedBackList.add(feedBack);
		feedBackData.setDeviceId("12d334");
		feedBackData.setFeedBack(feedBackList);
		String result = service.saveFeedBackDetails(feedBackData);
		log.error(result);
		assertEquals(true, result.contains("success"));
	}

	@Test
  public void testUrls() throws URISyntaxException, UnsupportedEncodingException{
    uriInfo = EasyMock.createMock(UriInfo.class);
    EasyMock.expect(uriInfo.getRequestUri())
        .andReturn(
            new java.net.URI(
                "https://cfa-dev.photoninfotech.com/cfa/services/product/getFilteredRecords?categoryId=cat45140773&start=0&rows=10"));
    EasyMock.replay(uriInfo);
    String result = service.getUrls(uriInfo, "6.1");
        log.error(result);
     
  }
	
	@Test
	public void testUrlsN() throws URISyntaxException, UnsupportedEncodingException{
	    uriInfo = EasyMock.createMock(UriInfo.class);
	    EasyMock.expect(uriInfo.getRequestUri())
	        .andReturn(
	            new java.net.URI(
	                "https://cfa-dev.photoninfotech.com/cfa/services/product/getFilteredRecords?categoryId=cat45140773&start=0&rows=10"));
	    EasyMock.replay(uriInfo);
	    String result = service.getUrls(uriInfo, "6.1");
	        log.error(result);
	     
	}
	
	@Test
	public void testUrlsIpad() throws URISyntaxException, UnsupportedEncodingException{
	    uriInfo = EasyMock.createMock(UriInfo.class);
	    EasyMock.expect(uriInfo.getRequestUri())
	        .andReturn(
	            new java.net.URI(
	                "https://cfa-dev.photoninfotech.com/cfa/services/product/getFilteredRecords?categoryId=cat45140773&start=0&rows=10"));
	    EasyMock.replay(uriInfo);
	    String result = service.getUrls(uriInfo, "6.1");
	        log.error(result);
	     
	}
	
	@Test
	public void testGetProfanityWords() throws URISyntaxException{
		EasyMock.expect(uriInfo.getRequestUri())
        .andReturn(new java.net.URI("https://cfa-dev.photoninfotech.com/cfa/services/product/getFilteredRecords?categoryId=cat45140773&start=0&rows=10"));
		EasyMock.replay(uriInfo);
		String result = service.getProfanityWords(uriInfo);
		log.error(result);
	}
	
	@Test
	public void testGetStateCodes() throws URISyntaxException{
		EasyMock.expect(uriInfo.getRequestUri())
        .andReturn(new java.net.URI("https://cfa-dev.photoninfotech.com/cfa/services/product/getFilteredRecords?categoryId=cat45140773&start=0&rows=10"));
		EasyMock.replay(uriInfo);
		Response result = service.getStateCodes(uriInfo);
		log.error(result.getEntity().toString());
	}
	
	@Test
	public void testsaveIdfaId() throws URISyntaxException{
		EasyMock.expect(uriInfo.getRequestUri())
        .andReturn(new java.net.URI("https://cfa-dev.photoninfotech.com/cfa/services/product/getFilteredRecords?categoryId=cat45140773&start=0&rows=10"));
		EasyMock.replay(uriInfo);
		String result = service.saveIdfaId("93B257E5-303A-4A7B-B917-68E5B921B055");
		log.error(result);
	}
	
	@Test
	public void testGetArticlesFeed() throws URISyntaxException, JSONException{
		EasyMock.expect(uriInfo.getRequestUri())
        .andReturn(new java.net.URI("https://cfa-dev.photoninfotech.com/cfa/services/product/getFilteredRecords?categoryId=cat45140773&start=0&rows=10"));
		EasyMock.replay(uriInfo);
		String result = service.getArticlesFeed("76.183.217.186,205.251.202.61");
		log.error(result);
	}
	
	@Test
	public void testGetWebViewData() throws URISyntaxException, JSONException{
		EasyMock.expect(uriInfo.getRequestUri())
        .andReturn(new java.net.URI("https://cfa-dev.photoninfotech.com/cfa/services/product/getFilteredRecords?categoryId=cat45140773&start=0&rows=10"));
		EasyMock.replay(uriInfo);
		String result = service.getWebViewData(uriInfo);
		log.error(result);
	}
	
	@Test
	public void testGetHideProperties() throws URISyntaxException, JSONException{
		EasyMock.expect(uriInfo.getRequestUri())
        .andReturn(new java.net.URI("https://cfa-dev.photoninfotech.com/cfa/services/product/getFilteredRecords?categoryId=cat45140773&start=0&rows=10"));
		EasyMock.replay(uriInfo);
		String result = service.getHideProperties(uriInfo, "[{\"name\":\"AGA\",\"attribs\":{\"expires\":\"Sat, 03-Feb-2085 13:07:28 GMT\",\"domain\":\"staging2.neimanmarcus.com\",\"path\":\"/\"},\"value\":\"T29000001:T29200002\",\"cookieDomain\":\"staging2.neimanmarcus.com\",\"cookieExpiryDate\":\"Feb 3, 2085 7:07:28 AM\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"BIGipServer~Test2~Staging2NeimansTest2\",\"attribs\":{\"path\":\"/\"},\"value\":\"419823626.63061.0000\",\"cookieDomain\":\"staging2.neimanmarcus.com\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"DYN_USER_CONFIRM\",\"attribs\":{\"expires\":\"Tue, 30-Jan-2085 17:50:04 +00:00\",\"path\":\"/\",\"domain\":\"staging2.neimanmarcus.com\"},\"value\":\"df43b8476bce21d5511b79b42f32526f\",\"cookieDomain\":\"staging2.neimanmarcus.com\",\"cookieExpiryDate\":\"Jan 30, 2085 11:50:04 AM\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"DYN_USER_ID\",\"attribs\":{\"expires\":\"Tue, 30-Jan-2085 17:50:04 +00:00\",\"path\":\"/\",\"domain\":\"staging2.neimanmarcus.com\"},\"value\":\"T2857041326\",\"cookieDomain\":\"staging2.neimanmarcus.com\",\"cookieExpiryDate\":\"Jan 30, 2085 11:50:04 AM\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"JSESSIONID\",\"attribs\":{\"path\":\"/\"},\"value\":\"YX50j13+3F-I46ejXwby9u-z\",\"cookieDomain\":\"staging2.neimanmarcus.com\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"TLTSID\",\"attribs\":{\"domain\":\".neimanmarcus.com\",\"path\":\"/\"},\"value\":\"6DEA752ED8D410D80CBC9E3AABBDAF0B\",\"cookieDomain\":\".neimanmarcus.com\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"TLTUID\",\"attribs\":{\"expires\":\"Thu, 12-01-2027 14:35:57 GMT\",\"domain\":\".neimanmarcus.com\",\"path\":\"/\"},\"value\":\"6DEA752ED8D410D80CBC9E3AABBDAF0B\",\"cookieDomain\":\".neimanmarcus.com\",\"cookieExpiryDate\":\"Jan 12, 2027 8:35:57 AM\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"TS01ab869c\",\"attribs\":{\"path\":\"/\",\"domain\":\"staging2.neimanmarcus.com\"},\"value\":\"01ebc3ab60901a98c39f2535f94d06b4532233d7bbb0edba972bd89939f6c56474b48e0852348bc82fc95b47d0bb4374d6d4d2915e6887d1af2902a673646c84905594a3ca8f09cff2acc3673d83c2b69dfa46d38cd1e6dee94538ed82abbd15bbf2e8c50cc6fd17828fbb75b26ca400da9a1ef3ad817b6b15883b60b46258282390d51234\",\"cookieDomain\":\"staging2.neimanmarcus.com\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"TS01c61dff\",\"attribs\":{\"path\":\"/\"},\"value\":\"01ebc3ab60e584887ad6b1d7bbb7ae98db3738def4053542dd2754d1a03d28321427927dcf099ac293de640aa95c41209bab899e8d87312192397672e64f29bb7bd348774f406ae8ee2044794113979d23a5b38a0e\",\"cookieDomain\":\"staging2.neimanmarcus.com\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"TS01f6f3ce\",\"attribs\":{\"domain\":\".neimanmarcus.com\",\"path\":\"/\"},\"value\":\"01ebc3ab60f1bb4548209585b716b4d1554715f9d2e7de8a4f34576dad52fb3888ce685e3408d4dbe0e5c40de41cc1d1044c57e4ee6ebab64d2f656c37385122bc87dd078f3d84f32c8ef53d19afe0e3c5ad6142d8\",\"cookieDomain\":\".neimanmarcus.com\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"W2A\",\"attribs\":{\"path\":\"/\"},\"value\":\"rd2o00000000000000000000ffff0a000eaco23030\",\"cookieDomain\":\"staging2.neimanmarcus.com\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"WID\",\"attribs\":{\"expires\":\"Tue, 30-Jan-2085 17:50:04 GMT\",\"path\":\"/\",\"domain\":\"staging2.neimanmarcus.com\"},\"value\":\"T2857041326\",\"cookieDomain\":\"staging2.neimanmarcus.com\",\"cookieExpiryDate\":\"Jan 30, 2085 11:50:04 AM\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"dtCookie\",\"attribs\":{\"path\":\"/\",\"domain\":\".neimanmarcus.com\"},\"value\":\"5DF761EA2659B3A9CCC002766BE545A2|X2RlZmF1bHR8MQ\",\"cookieDomain\":\".neimanmarcus.com\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0}]");
		log.error(result);
	}
	
	@Test
	public void testGetSessionIfo() throws URISyntaxException, JSONException{
		EasyMock.expect(uriInfo.getRequestUri())
        .andReturn(new java.net.URI("https://cfa-dev.photoninfotech.com/cfa/services/product/getFilteredRecords?categoryId=cat45140773&start=0&rows=10"));
		EasyMock.replay(uriInfo);
		BasicCookieStore cookieStore = new BasicCookieStore();
		AuthUtil.sessioninfo(cookieStore);
	}
	
	@Test
	public void testGetMiniCart() throws URISyntaxException, JSONException, UtilityException{
		EasyMock.expect(uriInfo.getRequestUri())
        .andReturn(new java.net.URI("https://cfa-dev.photoninfotech.com/cfa/services/product/getFilteredRecords?categoryId=cat45140773&start=0&rows=10"));
		EasyMock.replay(uriInfo);
		cartService.getMiniCart("[{\"name\":\"AGA\",\"attribs\":{\"expires\":\"Sat, 03-Feb-2085 13:07:28 GMT\",\"domain\":\"staging2.neimanmarcus.com\",\"path\":\"/\"},\"value\":\"T29000001:T29200002\",\"cookieDomain\":\"staging2.neimanmarcus.com\",\"cookieExpiryDate\":\"Feb 3, 2085 7:07:28 AM\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"BIGipServer~Test2~Staging2NeimansTest2\",\"attribs\":{\"path\":\"/\"},\"value\":\"419823626.63061.0000\",\"cookieDomain\":\"staging2.neimanmarcus.com\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"DYN_USER_CONFIRM\",\"attribs\":{\"expires\":\"Tue, 30-Jan-2085 17:50:04 +00:00\",\"path\":\"/\",\"domain\":\"staging2.neimanmarcus.com\"},\"value\":\"df43b8476bce21d5511b79b42f32526f\",\"cookieDomain\":\"staging2.neimanmarcus.com\",\"cookieExpiryDate\":\"Jan 30, 2085 11:50:04 AM\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"DYN_USER_ID\",\"attribs\":{\"expires\":\"Tue, 30-Jan-2085 17:50:04 +00:00\",\"path\":\"/\",\"domain\":\"staging2.neimanmarcus.com\"},\"value\":\"T2857041326\",\"cookieDomain\":\"staging2.neimanmarcus.com\",\"cookieExpiryDate\":\"Jan 30, 2085 11:50:04 AM\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"JSESSIONID\",\"attribs\":{\"path\":\"/\"},\"value\":\"YX50j13+3F-I46ejXwby9u-z\",\"cookieDomain\":\"staging2.neimanmarcus.com\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"TLTSID\",\"attribs\":{\"domain\":\".neimanmarcus.com\",\"path\":\"/\"},\"value\":\"6DEA752ED8D410D80CBC9E3AABBDAF0B\",\"cookieDomain\":\".neimanmarcus.com\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"TLTUID\",\"attribs\":{\"expires\":\"Thu, 12-01-2027 14:35:57 GMT\",\"domain\":\".neimanmarcus.com\",\"path\":\"/\"},\"value\":\"6DEA752ED8D410D80CBC9E3AABBDAF0B\",\"cookieDomain\":\".neimanmarcus.com\",\"cookieExpiryDate\":\"Jan 12, 2027 8:35:57 AM\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"TS01ab869c\",\"attribs\":{\"path\":\"/\",\"domain\":\"staging2.neimanmarcus.com\"},\"value\":\"01ebc3ab60901a98c39f2535f94d06b4532233d7bbb0edba972bd89939f6c56474b48e0852348bc82fc95b47d0bb4374d6d4d2915e6887d1af2902a673646c84905594a3ca8f09cff2acc3673d83c2b69dfa46d38cd1e6dee94538ed82abbd15bbf2e8c50cc6fd17828fbb75b26ca400da9a1ef3ad817b6b15883b60b46258282390d51234\",\"cookieDomain\":\"staging2.neimanmarcus.com\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"TS01c61dff\",\"attribs\":{\"path\":\"/\"},\"value\":\"01ebc3ab60e584887ad6b1d7bbb7ae98db3738def4053542dd2754d1a03d28321427927dcf099ac293de640aa95c41209bab899e8d87312192397672e64f29bb7bd348774f406ae8ee2044794113979d23a5b38a0e\",\"cookieDomain\":\"staging2.neimanmarcus.com\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"TS01f6f3ce\",\"attribs\":{\"domain\":\".neimanmarcus.com\",\"path\":\"/\"},\"value\":\"01ebc3ab60f1bb4548209585b716b4d1554715f9d2e7de8a4f34576dad52fb3888ce685e3408d4dbe0e5c40de41cc1d1044c57e4ee6ebab64d2f656c37385122bc87dd078f3d84f32c8ef53d19afe0e3c5ad6142d8\",\"cookieDomain\":\".neimanmarcus.com\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"W2A\",\"attribs\":{\"path\":\"/\"},\"value\":\"rd2o00000000000000000000ffff0a000eaco23030\",\"cookieDomain\":\"staging2.neimanmarcus.com\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"WID\",\"attribs\":{\"expires\":\"Tue, 30-Jan-2085 17:50:04 GMT\",\"path\":\"/\",\"domain\":\"staging2.neimanmarcus.com\"},\"value\":\"T2857041326\",\"cookieDomain\":\"staging2.neimanmarcus.com\",\"cookieExpiryDate\":\"Jan 30, 2085 11:50:04 AM\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0},{\"name\":\"dtCookie\",\"attribs\":{\"path\":\"/\",\"domain\":\".neimanmarcus.com\"},\"value\":\"5DF761EA2659B3A9CCC002766BE545A2|X2RlZmF1bHR8MQ\",\"cookieDomain\":\".neimanmarcus.com\",\"cookiePath\":\"/\",\"isSecure\":false,\"cookieVersion\":0}]", true);
	}
	
	@Test
	public void commonTest(){
		URLs urLs = new URLs();
		urLs.setAutoSuggest("");
		urLs.getAutoSuggest();
		urLs.setMiniCart("");
		urLs.getMiniCart();
		urLs.setMinCartCurrency("");
		urLs.getMinCartCurrency();
		urLs.setCheckOut("");
		urLs.getCheckOut();
		urLs.setCheckOutLogin("");
		urLs.getCheckOutLogin();
		urLs.setWishListShare("");
		urLs.getWishListShare();
		urLs.setCheckoutCss("");
		urLs.getCheckoutCss();
		urLs.setSurveyDay("");
		urLs.getSurveyDay();
		urLs.setWebViewUrls(new ArrayList<String>());
		urLs.getWebViewUrls();
		urLs.setSlyceKey("");
		urLs.getSlyceKey();
		urLs.setSlyceUrl("");
		urLs.getSlyceUrl();
		urLs.setDisplayHolidayHours("");
		urLs.getDisplayHolidayHours();
		urLs.setNetworkProfile("");
		urLs.getNetworkProfile();
		urLs.setSsid("");
		urLs.getSsid();
		urLs.setMyNM("");
		urLs.getMyNM();
		urLs.setXleftMaxCount(1);
		urLs.getXleftMaxCount();
		urLs.setBackgroundRadius("");
		urLs.getBackgroundRadius();
		urLs.setGeoFenceRefreshTime("");
		urLs.getGeoFenceRefreshTime();
		urLs.setStoreDetailsRefreshTime("");
		urLs.getStoreDetailsRefreshTime();
		urLs.setFavoriteItem("");
		urLs.getFavoriteItem();
		urLs.setPayMyBill("");
		urLs.getPayMyBill();
		urLs.setIncircleNeedHelp("");
		urLs.getIncircleNeedHelp();
		urLs.setIncircleApplyNow("");
		urLs.getIncircleApplyNow();
		urLs.setOrderHistory("");
		urLs.getOrderHistory();
		urLs.setAddressBook("");
		urLs.getAddressBook();
		urLs.setPaymentInfo("");
		urLs.getPaymentInfo();
		urLs.setIndicatorUrls(new ArrayList<String>());
		urLs.getIndicatorUrls();
		urLs.setEnableExpressCheckout("");
		urLs.getExpressCheckoutText();
		urLs.setEnableExpressCheckout("");
		urLs.getEnableExpressCheckout();
		urLs.setTwoCCOrderReviewUrl("");
		urLs.getTwoCCOrderReviewUrl();
		urLs.setExpressCheckoutUrl("");
		urLs.getExpressCheckoutUrl();
		urLs.setEditAccountURL("");
		urLs.getEditAccountURL();
		urLs.setWebViewEnviromentURL("");
		urLs.getWebViewEnviromentURL();
		urLs.setTwoCCFlowURLs(new ArrayList<String>());
		urLs.getTwoCCFlowURLs();
		urLs.setCheckOutWebviewStyles(new ArrayList<String>());
		urLs.getCheckOutWebviewStyles();
		urLs.setAssistanceUrl("");
		urLs.getAssistanceUrl();
		urLs.setPunchOutWebViewUrls(new ArrayList<String>());
		urLs.getPunchOutWebViewUrls();
	}
	
}
