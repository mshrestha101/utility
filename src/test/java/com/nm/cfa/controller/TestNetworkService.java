package com.nm.cfa.controller;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.nm.service.UtilityContoller;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:junit-cfa-servlet.xml" })
public class TestNetworkService extends TestCase {
	private static Logger log = Logger.getLogger(TestNetworkService.class);
	@Autowired
	UtilityContoller service;
	
	@Test
	public void testNetworkTypes() throws JSONException{
		Response store = service.getNetworkTypes();
		log.error(store.getEntity());
		
	}
	
}