package com.nm.cfa.controller;

import org.junit.BeforeClass;

public class TestCase {
	
	@BeforeClass
    public static void oneTimeSetUp() {
	    System.setProperty("cfa_env", "Staging1");
	    System.setProperty("memcacheURL", "172.16.18.169");
	    System.setProperty("checkoutURL", "https://staging.neimanmarcus.com/checkout/cart.jsp");	    
    }
}
