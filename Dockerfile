FROM tomcat:7.0.90-jre7

MAINTAINER Neiman <NMMobile_Team@neimanmarcus.com>

ENV ADMIN_USER admin
ENV ADMIN_PASSWORD 123@admin

USER root

RUN mkdir -p /opt/keystore/afa
COPY ./configurations/afa.jks /opt/keystore/afa

RUN mkdir -p /opt/cfa/properties
COPY ./configurations/cfa.properties /opt/cfa/properties

COPY ./configurations/server.xml /usr/local/tomcat/conf
COPY ./configurations/catalina.properties /usr/local/tomcat/conf
COPY ./configurations/mysql-connector-java-5.1.18.jar /usr/local/tomcat/lib

ADD ./target/utilityv2.war /usr/local/tomcat/webapps

CMD ["catalina.sh", "run"]a